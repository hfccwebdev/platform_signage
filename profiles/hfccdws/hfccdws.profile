<?php
/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function system_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = 'hfccdws';
}
/**
 * Implements hook_form_alter().
 */
function system_form_install_select_profile_form_alter(&$form, $form_state) {
  // select hfccdws install profile by default
  foreach ($form['profile'] as $key => $element) {
    $form['profile'][$key]['#value'] = 'hfccdws';
  }
}
/**
 * Implements hook_form_FORM_ID_alter().
 */
function hfccdws_form_install_configure_form_alter(&$form, $form_state) {
  $form['server_settings']['date_default_timezone']['#default_value'] = 'America/Detroit';
  unset($form['server_settings']['date_default_timezone']['#attributes']);
}