<?php

/**
 * @file
 * This file provides theme override functions for the HFCC theme.
 */

/**
 * Implements hook_form_alter()
 */
function divsites_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {
    unset($form['search_block_form']['#title']);
    // unset($form['actions']['submit']);
    unset($form['#attributes']['class']['0']);
    $form['search_block_form']['#title_display'] = 'invisible';
    $form_default = t('');
    $form['search_block_form']['#default_value'] = $form_default;
    $form['search_block_form']['#attributes'] = array(
      'onblur' => "if (this.value == '') {this.value = '';}", 
      'onfocus' => "if (this.value == '') {this.value = '';}"
    );
    $form['actions']['submit']['#value'] = t(' ');
  }
}
?>