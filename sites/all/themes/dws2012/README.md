Henry Ford Community College DWS2012 Theme 2.x for Drupal 7.x
-------------------------------------------------------------

Maintainers:

* Donald Dille <ddille@hfcc.edu>
* Micah Webner <micah@hfcc.edu>

This theme was originally adapted from early versions of the Basic theme
developed by Raincity Studios found at http://drupal.org/project/basic

The current version of this theme relies heavily on the Gridless HTML5 and
CSS3 boilerplate from http://thatcoolguy.github.com/gridless-boilerplate/

_____________________________________________________________________________

Installation

- Unpack the downloaded file and place the hfccbase folder in your Drupal
  installation under one of the following locations:

    * sites/all/themes
    * sites/all/custom/themes
    * sites/default/themes
    * sites/example.com/themes
    * profiles/customprofile/themes

- Log in as an administrator on your Drupal site and go to
  Administer > Site building > Themes (admin/build/themes) and enable the
  hfccbase theme.
