<?php

/**
 * @file
 * This file provides theme override functions for the dws2012 theme.
 */

/**
 * Implements hook_template_preprocess_html().
 *
 * Preprocess variables for html.tpl.php.
 */
function dws2012_preprocess_html(&$variables) {

  // Add viewport metatag for mobile devices.
  $meta_viewport = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1.0',
    ),
  );
  drupal_add_html_head($meta_viewport, 'meta_viewport');

  // Set up IE meta tag to force IE rendering mode.
  $meta_ie_render_engine = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' => 'X-UA-Compatible',
      'content' =>  'IE=edge,chrome=1',
    ),
  );
  drupal_add_html_head($meta_ie_render_engine, 'meta_ie_render_engine');

  // Add Google Fonts.
  drupal_add_css('//fonts.googleapis.com/css?family=Arimo:400,400italic,700', array('group' => CSS_THEME, 'type' => 'external'));

  // Additional classes for body element.
  if (user_access('administer site configuration')) {
    $variables['classes_array'][] = 'admin';
  }

  // Add classes for website section.
  if (!$variables['is_front']) {
    $path = drupal_get_path_alias($_GET['q']);
    if (arg(0) == 'node' && arg(1) == 'add') {
      $variables['classes_array'][] = 'section-node-add';
    }
    elseif (arg(0)== 'node' && is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
      $variables['classes_array'][] = 'section-node-' . arg(2); // Add 'section-node-edit' or 'section-node-delete'
    }
    else {
      list($section, ) = explode('/', $path, 2);
      $variables['classes_array'][] = dws2012_id_safe('section-' . $section);
    }
  }
}

/**
 * Implements hook_template_preprocess_page().
 *
 * Preprocess variables for page.tpl.php.
 */
function dws2012_preprocess_page(&$variables) {
  if (module_exists('search')) {
    $variables['search_box'] = search_block_view();
  }
}

/**
 * Implements hook_template_preprocess_node().
 *
 * Preprocess variables for node.tpl.php.
 */
function dws2012_preprocess_node(&$variables) {
  global $user;
  if ($variables['node']->uid && $variables['node']->uid == $user->uid) {
    $variables['classes_array'][] = 'node-mine';
  }
}

/**
 * Converts a string to a suitable html ID attribute.
 *
 * http://www.w3.org/TR/html4/struct/global.html#h-7.5.2 specifies what makes a
 * valid ID attribute in HTML. This function:
 *
 *   - Ensure an ID starts with an alpha character by optionally adding an 'n'.
 *   - Replaces any character except A-Z, numbers, and underscores with dashes.
 *   - Converts entire string to lowercase.
 *
 * @param $string
 *   The string.
 * @return
 *   The converted string.
 */
function dws2012_id_safe($string) {
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  $string = drupal_strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '-', $string));
  // If the first character is not a-z, add 'n' in front.
  if (!ctype_lower($string{0})) { // Don't use ctype_alpha since its locale aware.
    $string = 'id' . $string;
  }
  return $string;
}

/**
 * Implements hook_template_preprocess_entity().
 */
function dws2012_preprocess_entity(&$variables) {
  if ($variables['entity_type'] == 'bean' && !empty($variables['elements']['#bundle']) && $variables['elements']['#bundle'] == 'anywhere_button') {
    $imagevars = array('path' => $variables['field_button_image'][0]['uri']);
    if (!empty($variables['field_button_target'][0]['title'])) {
      $imagevars['alt'] = $variables['field_button_target'][0]['title'];
      $imagevars['title'] = $variables['field_button_target'][0]['title'];
    }
    $image = theme('image', $imagevars);
    $variables['content']['formatted_button'] = array(
       '#markup' =>l($image, $variables['field_button_target'][0]['url'], array('html' => TRUE)),
    );
    unset($variables['content']['field_button_target']);
    unset($variables['content']['field_button_image']);
  }
}

/**
 * Implements hook_template_preprocess_field().
 */
function dws2012_preprocess_field(&$variables, $hook) {

  // Specify alternate templates for selected fields.

  switch ($variables['element']['#field_name']) {
    case 'field_program_xfer_schools':
      $variables['theme_hook_suggestions'][] = 'field__list';
      $variables['field_list_type'] = 'ul';
      break;
  }
}

/**
 * Implements hook_form_alter()
 */
function hfcc_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {
    unset($form['search_block_form']['#title']);
    // unset($form['actions']['submit']);
    unset($form['#attributes']['class']['0']);
    $form['search_block_form']['#title_display'] = 'invisible';
    $form['search_block_form']['#size'] = 13;
    $form_default = t('');
    $form['search_block_form']['#default_value'] = $form_default;
    $form['search_block_form']['#attributes'] = array(
      'onblur' => "if (this.value == '') {this.value = '';}", 
      'onfocus' => "if (this.value == '') {this.value = '';}"
    );
    $form['actions']['submit']['#value'] = t(' ');
  }
}
?>
